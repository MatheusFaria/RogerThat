﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="CallTheCops/AudioMessage")]
public class AudioMessage : ScriptableObject {
    public List<AudioClipMessage> clips;
}
