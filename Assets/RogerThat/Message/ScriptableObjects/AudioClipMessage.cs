﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="CallTheCops/AudioClipMessage")]
public class AudioClipMessage : ScriptableObject {
    public AudioClip clip;
    public AudioClip clipRev;
	public int partNumber = 0;
}
