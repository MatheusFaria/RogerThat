﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageHub : MonoBehaviour {

	public delegate void MessageHubListener (MessageHub messageHub);
	public MessageHubListener onMessageQueueEmpty;


	private const float CANT_PLOT_TIME = -1f;
	private AudioSource audioSource;

    public float timeToPlot = 5f;
	[HideInInspector] public Queue<Message> messageQueue;


	private float startedPlotTime = CANT_PLOT_TIME;

    private Transform slots;
    private Transform messageSpot;
	private Message[] allMessages;

	private bool HasMessage { get { return this.messageQueue != null && this.messageQueue.Count > 0; } }

	void Awake () {
		this.audioSource = GetComponent<AudioSource> ();
		this.startedPlotTime = CANT_PLOT_TIME;
	}

    void Start() {
        this.slots = transform.Find("Slots");
        this.messageSpot = transform.Find("MessageSpot");
    }

	void Update () {
		CheckPlotMessage ();
	}

	public void AddMessages(Queue<Message> messageQueue) {
		ClearMessages ();

        this.messageQueue = messageQueue;

		int temperedMessages = 2;

		int x = 0;
		foreach (Message message in this.messageQueue) {
			message.transform.SetParent(slots);
			message.gameObject.SetActive(false);

			if (temperedMessages > 0 && (Random.Range (0f, 1f) >= 0.5f || x == (this.messageQueue.Count - temperedMessages))) {
				temperedMessages--;
				message.SetNumber (0);
			} else {
				message.SetNumber ();
			}

			message.onPick += OnMessagePicked;
			x++;
		}



		for (int j = 0; j < this.messageQueue.Count; ++j) {
			int limit = Random.Range (0, this.messageQueue.Count);
			Message m = this.messageQueue.Dequeue ();

			for (int i = 0; i < limit; ++i) {
				this.messageQueue.Enqueue (this.messageQueue.Dequeue ());
			}
			this.messageQueue.Enqueue (m);
		}

		this.allMessages = this.messageQueue.ToArray ();
		StartTimeToPlot ();
    }

	public void StartTimeToPlot () {
		if(this.HasMessage)
			this.startedPlotTime = Time.time;
	}

	private void ClearMessages () {
		if (this.allMessages == null) return;

		for (int i = 0; i < this.allMessages.Length; i++) {
			GameObject.Destroy (this.allMessages [i].gameObject);
		}

		this.messageQueue.Clear ();
	}

	private void CheckPlotMessage () {
		if (!CanPlotMessage ()) return;

		PlotMessage ();
	}

	private bool CanPlotMessage () {
		bool finishCooldown = this.startedPlotTime != CANT_PLOT_TIME && Time.time >= this.startedPlotTime + this.timeToPlot;
		
		return this.HasMessage && finishCooldown;
	}

	private void PlotMessage () {
		this.startedPlotTime = CANT_PLOT_TIME;

		Message message = this.messageQueue.Dequeue ();

		message.gameObject.SetActive (true);
		message.gameObject.transform.position = this.messageSpot.transform.position;
		message.gameObject.transform.rotation = this.messageSpot.transform.rotation;

		this.audioSource.Play ();
	}

	private void NotifyHasNoMessagesOnQueue () {
		if (!this.HasMessage && this.onMessageQueueEmpty != null)
			this.onMessageQueueEmpty (this);
	}

	private void OnMessagePicked (Message message) {
		StartTimeToPlot ();
		NotifyHasNoMessagesOnQueue ();

		message.onPick -= OnMessagePicked;
	}
}
