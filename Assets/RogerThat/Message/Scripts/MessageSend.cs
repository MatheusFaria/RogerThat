﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageSend : MonoBehaviour {

	public static int levelPush = Animator.StringToHash("LevelPush");


	public Slotable[] slotables;
	public int score = 100;
	public int penality = 50;
	public Animator lightsAnimator;

	public Animator animator;

	public AudioClip victoryClip;
	public AudioClip defeatClip;

	private AudioSource audioSource;


	void Awake() {
		this.audioSource = GetComponent<AudioSource> ();
	}

	public void Send() {
		if (!GameManager.instance.matchRunning) {
			return;
		}

		this.animator.SetTrigger(levelPush);

		int rightCount = 0;

		for (int i = 1; i <= slotables.Length; ++i) {
			Slotable slot = slotables [i - 1];

			if (slot.Empty) {
				OnNotEnoughMessages ();
				return;
			}

			if (!slot.Empty && slot.message.isFixed && slot.message.audioClipMessage.partNumber == i) {
				rightCount++;
			}
		}

		if (rightCount == slotables.Length) {
			OnCorrectMessage ();
		} else {
			OnWrongMessage ();
		}
	}

	private void OnCorrectMessage() {
		lightsAnimator.SetTrigger ("Green");

		GameManager.instance.score += score;

		for (int i = 1; i <= slotables.Length; ++i) {
			Slotable slot = slotables [i - 1];

			slot.message.OnRelase ();
			slot.message.gameObject.SetActive (false);
			Destroy (slot.message);
			slot.message = null;
		}

		GameManager.instance.GenerateNewMessageList ();

		this.audioSource.clip = victoryClip;
		this.audioSource.Play ();
	}

	private void OnWrongMessage() {
		lightsAnimator.SetTrigger ("Red");
		GameManager.instance.score -= penality;

		this.audioSource.clip = defeatClip;
		this.audioSource.Play ();
	}

	private void OnNotEnoughMessages() {
	}
}
