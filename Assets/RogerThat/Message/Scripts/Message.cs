﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Message : MonoBehaviour {

	public delegate void MessageListener(Message message);
	public MessageListener onPick;

    public bool isDefect = false;
	public bool isFixed = false;
    public AudioClipMessage audioClipMessage;

	public int number = -1;

    public bool isLowVolume;
    public bool isPitched;
    public bool isNoised;
    public bool isReversed;

    public bool pitchedUp;

    private new Rigidbody rigidbody;
    private new BoxCollider collider;

    private AudioSource audioSource;
    private AudioMixerGroup mixerGroup;

    void Awake() {
        this.rigidbody = GetComponent<Rigidbody>();
        this.collider = GetComponent<BoxCollider>();
    }

    void Start() {
        this.audioSource = GetComponent<AudioSource>();

		if (number == -1) this.SetNumber(this.audioClipMessage.partNumber);

        RandomizeEffects();
    }

	public void OnPick(Transform destiny) {
        this.collider.enabled = false;
        this.rigidbody.isKinematic = true;

        this.transform.SetParent(destiny);

        this.transform.localPosition = Vector3.zero;
        this.transform.localRotation = Quaternion.identity;

		if (this.onPick != null) this.onPick (this);
    }

    public void OnRelase() {
        this.collider.enabled = true;
        this.rigidbody.isKinematic = false;

        this.transform.SetParent(null);
    }

	public void SetNumber(int n=-1) {
		if (n == -1)
			n = this.audioClipMessage.partNumber;
		this.number = n;
		transform.Find ("Model").GetChild (n).gameObject.SetActive(true);
	}

    private void RandomizeEffects () {
        isLowVolume = Random.Range(0f,1f) > 0.5 ? true : false;
        isPitched = Random.Range(0f,1f) > 0.5 ? true : false;
        isNoised = Random.Range(0f,1f) > 0.5 ? true : false;
        isReversed = Random.Range(0f,1f) > 0.5 ? true : false;
        pitchedUp = Random.Range(0f,1f) > 0.5 ? true : false;
        SetMixer();
    }

	public void SetMixer () {
        if (audioSource.isPlaying) {
            audioSource.Stop();
            Debug.Log("ERROR? switching mixer while playing");
        }

        int effectsCounter = 0;
        int index = 0;

        if (isLowVolume)
            effectsCounter++;
        if (isPitched)
            effectsCounter++;
        if (isNoised)
            effectsCounter++;
        if (isReversed)
            effectsCounter++;

        switch (effectsCounter) {
            case 0: {
                index = 0;
				this.isFixed = true;
                break;
            }
            case 1: {
                if (isLowVolume)
                    index = 1;
                else if (isPitched)
                    index = 2;
                else if (isNoised)
                    index = 3;
                else if (isReversed)
                    index = 4;
                break;
            }
            case 2: {

                if (isLowVolume && isPitched)
                    index = 5;
                else if (isLowVolume && isNoised)
                    index = 6;
                else if (isLowVolume && isReversed)
                    index = 7;
                else if (isPitched && isNoised)
                    index = 8;
                else if (isPitched && isReversed)
                    index = 9;
                else if (isNoised && isReversed)
                    index = 10;
                break;
            }
            case 3: {
                if (isLowVolume && isPitched && isNoised)
                    index = 11;
                else if (isLowVolume && isPitched && isReversed)
                    index = 12;
                else if (isPitched && isNoised && isReversed)
                    index = 13;
                break;
            }
            case 4: {
                index = 14;
                break;
            }
        }

        this.mixerGroup = AudioMain.Instance.GetMixerGroup(index);
        audioSource.outputAudioMixerGroup = this.mixerGroup;
	}

    public void Play() {
        if (!isReversed)
            audioSource.clip = audioClipMessage.clip;
        else
            audioSource.clip = audioClipMessage.clipRev;

        if (isPitched) {
            if (pitchedUp)
                mixerGroup.audioMixer.SetFloat(GetPitchMixerName(mixerGroup), 4f);
            else
                mixerGroup.audioMixer.SetFloat(GetPitchMixerName(mixerGroup), 0.5f);
        }

        audioSource.Play();
    }

    public void Stop() {
        audioSource.Stop();
    }

    string GetPitchMixerName(AudioMixerGroup audioMixerGroup) {
        Debug.Log(audioMixerGroup.name + "_pitch");
        return audioMixerGroup.name + "_pitch";
    }

    public bool isPlaying() {
        if (audioSource != null)
            return audioSource.isPlaying;

        return false;
    }
}
