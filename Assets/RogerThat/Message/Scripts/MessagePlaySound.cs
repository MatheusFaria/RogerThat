﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessagePlaySound : MonoBehaviour {

	public static int pressButton = Animator.StringToHash("PressButton");


	public Animator animator;
	public Slotable[] slotables;
	public GameObject[] lights;
	public Animator lightsAnimator;

//	public bool IsPlaying { get { return this.audioClipQueue.Count > 0 || this.audioSource.isPlaying; } }

	private int currentSlotableIndex;
	private AudioSource audioSourceEmptySlot;


	void Awake () {
		this.currentSlotableIndex = -1;
		this.audioSourceEmptySlot = GetComponent<AudioSource> ();
	}

	void LateUpdate () {
		if (CanPlayNextClip ())
			PlayNextClip ();
	}

	public void Play () {
		this.lightsAnimator.enabled = false;
		this.currentSlotableIndex = -1;

		for (int i = 0; i < this.slotables.Length; i++) {
			this.slotables [i].canRemove = false;

			if(this.slotables[i].message != null)
				this.slotables [i].message.Stop ();
		}
		this.audioSourceEmptySlot.Stop ();
		this.animator.SetTrigger(pressButton);

		PlayNextClip ();
	}

	private void PlayNextClip () {
		if (this.currentSlotableIndex >= 0) {
			this.slotables [this.currentSlotableIndex].canRemove = true;
			this.lights [this.currentSlotableIndex].transform.Find ("Off").gameObject.SetActive (true);
			this.lights [this.currentSlotableIndex].transform.Find ("Warn").gameObject.SetActive (false);
		}

		this.currentSlotableIndex++;

		bool finishSlots = this.currentSlotableIndex >= this.slotables.Length;
		if (finishSlots) {
			FinishPlay ();
			return;
		}

		Message message = this.slotables [this.currentSlotableIndex].message;

		if (message != null)
			AudioMain.Instance.MachinePlay (message);
		else
			this.audioSourceEmptySlot.Play ();

		this.lights [this.currentSlotableIndex].transform.Find ("Off").gameObject.SetActive (false);
		this.lights [this.currentSlotableIndex].transform.Find ("Warn").gameObject.SetActive (true);
	}

	private bool CanPlayNextClip () {
		if (this.currentSlotableIndex == -1) return false;

		Message message = this.slotables [this.currentSlotableIndex].message;

		if (message != null)
			return !message.isPlaying ();
		else
			return !this.audioSourceEmptySlot.isPlaying;
	}

	private void FinishPlay () {
		this.lightsAnimator.enabled = true;
		this.currentSlotableIndex = -1;
	}
}
