﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageDrop : MonoBehaviour {

	public int penality = 30;

	public void DropAllMessages () {
		GameManager.instance.score -= penality;
		GameManager.instance.GenerateNewMessageList ();
	}
}
