﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour {

    [SerializeField]
    Text tutorialText;

    [SerializeField]
    string [] tutorialStrings = {
        "Hey Rookie! It's time for you to finally do something that matters in your life, you good for nothing robocop!",
		"This is the most advanced CSI, audio decoder and shenaniganizer department in the entire world",
		"And your job here is to fix the stupid messages that keep getting damaged, becoming unhearable!",
		"These stupid messages come fragmented in parts, and it is your job to order them in the correct way!",
		"But, wait, wait... That's not the only problem... or problems... that these messages can have.",
		"You have a lot of machines here... to help you out.",
		"Get the fragments of the message from here, the message receiver.",
		"This is the Slots machine. Here is where you put and organize the fragments in order, from left to right. There are 5 fragments",
		"You can play the messages with the Play Button, right here. It will play nothing on empty slots.",
		"But before trying to order these fragments, you have to check if each one of them is defectuous:",
		"If the audio is too fast and high pitched or too slow and low pitched, bring it to this one, the PSCM (Pitch/Speed Corrector Machine)",
		"If the audio has a lot of random noise, bring it to this one, the NRM (Noise Removal Machine)",
		"These other 2 corrects very low volumes or inverted messages, but I don`t remember which one is what, so you figure it out yourself.",
		"After waiting the machine output your message, just grab it, bring it to the Slot Machine, play it, and see if there are no more problems with them.",
		"After fixing them and organizing them in order, send them with this lever here, and then start over with another message.",
		"But be fast and professional! I don`t want any sent message with something incorrect or for you to take all eternity doing this.",
    };

    [SerializeField]
    GameObject MessageReceiverHighlight;

    [SerializeField]
    GameObject SlotMachineHighlight;

    [SerializeField]
    GameObject PlayerMachineHighlight;

    [SerializeField]
    GameObject PitchMachineHighlight;

    [SerializeField]
    GameObject NoiseMachineHighlight;

    [SerializeField]
    GameObject VolumeMachineHighlight;

    [SerializeField]
    GameObject ReverseMachineHighlight;

    [SerializeField]
    GameObject SendMachineHighlight;

    [SerializeField]
    AudioSource voice;

    [SerializeField]
    AudioClip []randomSpeeches;

    [SerializeField]
    MeshRenderer screenMeshRenderer;

    [SerializeField]
    Texture texture1;

    [SerializeField]
    Texture texture2;

    [SerializeField]
    Texture texture3;

    int messageIndex = 0;

    Coroutine coroutine;

    public void Start() {
        if (GameManager.instance.TutorialShown) {
            gameObject.SetActive(false);
        } else {
            OnNextMessage();        
        }
    }

	void Update() {
		if (Input.anyKeyDown) {
			OnNextMessage ();
		}
	}

    public void OnNextMessage() {
        if(messageIndex >= tutorialStrings.Length) {
            gameObject.SetActive(false);
            GameManager.instance.StartGame();
            return;
        }

        tutorialText.text = tutorialStrings[messageIndex];

        if(coroutine != null) {
            StopCoroutine(coroutine);

            screenMeshRenderer.sharedMaterials[2].mainTexture = texture2;
        }

        coroutine = StartCoroutine(SpeechLoop());

        SetGOsActive();

        messageIndex++;        
    }

    IEnumerator SpeechLoop () {       

        int indexLooper = 0;
        Texture texture = null;

        PlayRandomSpeech();

        while(voice.isPlaying) {            

            switch (Random.Range(0, 3)) {
                case 0:
                    texture = texture1;
                    break;
                case 1:
                    texture = texture2;
                    break;
                case 2:
                    texture = texture3;
                    break;
            }

            screenMeshRenderer.sharedMaterials[2].mainTexture = texture;
            indexLooper++;

            yield return new WaitForSeconds(0.1f);
        }

        screenMeshRenderer.sharedMaterials[2].mainTexture = texture2;
    }

    void PlayRandomSpeech () {
        voice.clip = randomSpeeches[Random.Range(0,randomSpeeches.Length)];
        voice.Play();
    }

    void SetGOsActive () {
        MessageReceiverHighlight.SetActive(false);
        SlotMachineHighlight.SetActive(false);
        PlayerMachineHighlight.SetActive(false);
        PitchMachineHighlight.SetActive(false);
        NoiseMachineHighlight.SetActive(false);
        VolumeMachineHighlight.SetActive(false);
        ReverseMachineHighlight.SetActive(false);
        SendMachineHighlight.SetActive(false);

        switch (messageIndex) {
            case 6:
                MessageReceiverHighlight.SetActive(true);
                break;
            case 7:
                SlotMachineHighlight.SetActive(true);
                break;
            case 8:
                PlayerMachineHighlight.SetActive(true);
                break;
            case 10:
                PitchMachineHighlight.SetActive(true);
                break;
            case 11:
                NoiseMachineHighlight.SetActive(true);
                break;
            case 12:
                VolumeMachineHighlight.SetActive(true);
                ReverseMachineHighlight.SetActive(true);
                break;
            case 14:
                SendMachineHighlight.SetActive(true);
                break;
        }
    }
}
