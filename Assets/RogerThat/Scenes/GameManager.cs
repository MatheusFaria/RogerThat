﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

    public List<AudioMessage> messages;
    public GameObject messagePrefab;
	public float secondsToStart = 3f;
	public float matchDuration = 300f;
	public int score = 0;

	public float CurrentMatchDuration { get { return this.currentMatchDuration; } }

	[HideInInspector] public bool matchRunning = false;

    private MessageHub messageHub;
	private bool canPressButtonToRestart = false;
	[SerializeField] private float currentMatchDuration;
	[SerializeField] private GameObject panelGameOverObject;

	private Animator cameraAnimator;
	[SerializeField] private GameObject gameOverScreen;

    public bool TutorialShown { get { return tutorialShown; } }
    private bool tutorialShown = false;

	void Awake () {
		if (instance == null) instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}

		DontDestroyOnLoad (gameObject);
	}

    public void StartGame() {
        this.tutorialShown = true;
		this.matchRunning = true;
		this.currentMatchDuration = this.matchDuration;
		this.score = 0;

        messageHub = GameObject.Find("MessageHub").GetComponent<MessageHub>();

		this.gameOverScreen = transform.Find ("GameOverScreen").gameObject;

		this.cameraAnimator = GameObject.Find ("CameraContainer").GetComponent<Animator> ();
		this.panelGameOverObject = GameObject.Find ("Canvas").transform.Find ("PanelGameOver").gameObject;

		GenerateNewMessageList ();
    }

	public void Update () {
		if (this.matchRunning) {
			this.currentMatchDuration -= Time.deltaTime;
			if (this.currentMatchDuration <= 0) {
				this.currentMatchDuration = 0f;
				EndMatch ();
			}
		}

		if (this.canPressButtonToRestart && Input.anyKeyDown) {
			this.canPressButtonToRestart = false;
			this.gameOverScreen.SetActive (false);
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			Invoke("StartGame", this.secondsToStart);
		}
	}

	public void GenerateNewMessageList () {
		messageHub.AddMessages(GetRandomMessageList());
	}

	private void EndMatch() {
		this.matchRunning = false;
		// this.panelGameOverObject.SetActive (true);
		this.gameOverScreen.SetActive (true);
		GameObject.Find ("Player").GetComponent<PlayerJoystick> ().enabled = false;

		this.cameraAnimator.SetBool ("Zoom", true);
		Invoke ("EnableCanPressAnyButtonToRestart", 2);
	}

    private Queue<Message> GetRandomMessageList() {
		AudioMessage message = this.messages[Random.Range(0, this.messages.Count)];

		Queue<Message> messageQueue = new Queue<Message>();
        for(int i = 0; i < message.clips.Count; ++i) {
            AudioClipMessage clip = message.clips[i];
            Message m = Instantiate(messagePrefab).GetComponent<Message>();
			m.audioClipMessage = clip;

			messageQueue.Enqueue (m);
        }

        return messageQueue;
    }

	private void EnableCanPressAnyButtonToRestart () {
		this.canPressButtonToRestart = true;
	}
}
