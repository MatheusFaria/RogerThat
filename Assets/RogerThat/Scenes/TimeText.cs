﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimeText : MonoBehaviour {
	private Text text;

	void Awake() {
		text = GetComponent<Text> ();
	}

	void LateUpdate() {
		int time = (int) GameManager.instance.CurrentMatchDuration;
		text.text = (time/60).ToString("D2") + ":" + (time%60).ToString("D2");
	}
}
