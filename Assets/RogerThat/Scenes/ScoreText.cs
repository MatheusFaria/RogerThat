﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour {
	private Text text;

	void Awake() {
		text = GetComponent<Text> ();
	}

	void LateUpdate() {
		text.text = GameManager.instance.score.ToString();
	}
}
