﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slotable : MonoBehaviour {

	public Transform slot;

	public delegate void SlotableListner();
	public SlotableListner onInsertMessage;

	public AudioClip putMessageClip;
	public AudioClip getMessageClip;

	public bool Empty { get { return this.message == null; }}
	public bool canRemove { get { return this.mcanRemove && this.message != null; } set { this.mcanRemove = value; }}

	[HideInInspector] public Message message;
	[HideInInspector] public bool mcanRemove = true;

	private AudioSource audioSource;

	public void InsertMessage(Message message) {
		this.message = message;
		this.message.OnPick(slot);

		PlayPutInSound ();

		if (this.onInsertMessage != null) {
			this.onInsertMessage ();
		}
	}

	void Awake() {
		this.audioSource = GetComponent<AudioSource> ();
	}

	public void PlayPutInSound() {
		this.audioSource.clip = putMessageClip;
		this.audioSource.Play ();
	}

	public void PlayGetOutSound() {
		this.audioSource.clip = getMessageClip;
		this.audioSource.Play ();
	}
}
