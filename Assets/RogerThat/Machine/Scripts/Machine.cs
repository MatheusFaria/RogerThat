﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Machine : MonoBehaviour {
    public enum MachineType {
        volume,
        pitch,
        noise,
        reverse,
    }

	public int score = 20;
	public int penality = 15;
    public float timeToProcess = 2f;

    public MachineType machineType;

	public AudioClip processMessageClip;
	public Animator machineAnimator;

    public bool Empty { get { return this.slotable.message == null; }}
	public bool Processing { get { return !this.slotable.mcanRemove; }}
	public bool Finished { get { return this.slotable.canRemove && this.slotable.message != null; }}

    private float elapsedTimestamp = 0f;

	private Slotable slotable;
	private AudioSource audioSource;

	//private float maskWidth;
	//private GameObject progressCanvasObject;
	//private RectTransform maskRectTransform;

	void Awake() {
		this.audioSource = GetComponent<AudioSource> ();
	}

    void Start() {
		this.slotable = GetComponent<Slotable> ();
		this.slotable.onInsertMessage += InsertMessage;

		//this.progressCanvasObject = transform.Find ("ProcessCanvas").gameObject;
		//this.maskRectTransform = this.progressCanvasObject.transform.Find ("Mask").GetComponent<RectTransform> ();

		//this.progressCanvasObject.SetActive (false);
		//this.maskWidth = this.maskRectTransform.rect.width;
    }

    void Update() {
		if (Processing) {
            ProcessMessage();

            if (elapsedTimestamp >= timeToProcess) {
                OutputMessage();
            }
        }
    }

    public void InsertMessage() {
		this.elapsedTimestamp = 0f;
		this.slotable.canRemove = false;

		this.machineAnimator.SetBool ("Process", true);
		//this.progressCanvasObject.SetActive (true);
		//SetProgressMaskSize ();
    }

    public void OutputMessage() {
        this.slotable.canRemove = true;
		bool accept = AudioMain.Instance.MachineDeliver(this.machineType, this.slotable.message);

		if (accept) {
			GameManager.instance.score += this.score;

		} else {
			this.slotable.message.isDefect = false;
			GameManager.instance.score -= this.penality;
		}

		this.audioSource.clip = processMessageClip;
		this.audioSource.Play ();

		// this.progressCanvasObject.SetActive (false);
		this.machineAnimator.SetBool ("Process", false);
    }

    public void ProcessMessage() {
       this.elapsedTimestamp += Time.deltaTime;

		//SetProgressMaskSize ();
    }

	//private void SetProgressMaskSize () {
	//	float percentage = this.elapsedTimestamp / this.timeToProcess;
	//	percentage = Mathf.Min (percentage, 1f);

		//this.maskRectTransform.sizeDelta = new Vector2 (this.maskWidth * percentage, this.maskRectTransform.sizeDelta.y);
	//}
}
