﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioMain : MonoBehaviour {

    private static AudioMain instance;

    public static AudioMain Instance {
        get {
            if (instance == null) {
                AudioMain aux_instance = FindObjectOfType<AudioMain>();
                if (aux_instance == null) {
                    Debug.LogError("Couldn't find any Audio instances in the scene");
                } else {
                    aux_instance.Awake();
                }
            }
            return instance;
        }
    }

    private void Awake() {
        if (instance != null && instance != this) {
            Debug.LogWarning("Destroyed duplicate Audio instance " + gameObject.name);
            Destroy(this.gameObject);
        } else {
            instance = this;
        }

        //DontDestroyOnLoad(transform);
    }

    [SerializeField]
    AudioMixerGroup []effectsMixers = new AudioMixerGroup[14];

    [SerializeField]
    AudioSource audioDefect;

    [SerializeField]
    AudioSource audioNoise;

    Coroutine noiseCoroutine;

	public bool MachineDeliver(Machine.MachineType machineType, Message audioMessage) {
        switch(machineType) {
            case Machine.MachineType.volume: {
                    return MachineVolume(audioMessage);
                }
            case Machine.MachineType.pitch: {
                    return MachinePitch(audioMessage);
                }
            case Machine.MachineType.noise: {
                    return MachineNoise(audioMessage);
                }
            case Machine.MachineType.reverse: {
                    return MachineReverse(audioMessage);
                }
        }

		return false;
    }

	// Use this for initialization
	public void MachinePlay (Message audioMessage) {
        if (!audioMessage.isDefect) {
            audioMessage.Play();
            if (audioMessage.isNoised) {
                if (noiseCoroutine != null)
                    StopCoroutine (noiseCoroutine);
                noiseCoroutine = StartCoroutine(NoisePlayer(audioMessage));
            }
        }
        else {
            audioDefect.Play();
        }
	}

    IEnumerator NoisePlayer(Message audioMessage) {
        audioNoise.Play();
        while (audioMessage.isPlaying()) {
            yield return new WaitForEndOfFrame();
        }

        audioNoise.Stop();
    }

    	// Use this for initialization
	public bool MachineVolume (Message audioMessage) {
        if (audioMessage.isLowVolume) {
            audioMessage.isLowVolume = false;
            audioMessage.SetMixer();
			return true;

        } else {
			audioMessage.isDefect = true;
			return false;
        }
	}

    	// Use this for initialization
	public bool MachinePitch (Message audioMessage) {
        if (audioMessage.isPitched) {
            audioMessage.isPitched = false;
			audioMessage.SetMixer();
			return true;

        } else {
			audioMessage.isDefect = true;
			return false;
        }
	}

    	// Use this for initialization
	public bool MachineNoise (Message audioMessage) {
        if (audioMessage.isNoised) {
            audioMessage.isNoised = false;
			audioMessage.SetMixer();
			return true;

        } else {
			audioMessage.isDefect = true;
			return false;
        }
	}

    	// Use this for initialization
	public bool MachineReverse (Message audioMessage) {
        if (audioMessage.isReversed) {
            audioMessage.isReversed = false;
			audioMessage.SetMixer();
			return true;
        } else {
			audioMessage.isDefect = true;
			return false;
        }
	}

    public AudioMixerGroup GetMixerGroup(int index) {
        return effectsMixers[index];
    }
}
