﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util {

	public static bool isAnimationRunning(Animator animator, string stateName, string layer="Base Layer") {
		return animator.GetCurrentAnimatorStateInfo (animator.GetLayerIndex(layer)).IsName (stateName);
	}

	public static bool inMySight(Vector3 eyePosition, Vector3 lookDirection, Vector3 targetPosition, float angle=180f) {
		Vector3 direction = targetPosition - eyePosition;
		direction.Normalize ();

		float dot = Vector3.Dot (lookDirection, direction);
		return dot >= Mathf.Cos(Mathf.Deg2Rad * angle * 0.5f);
	}

	public static int[] GetIntegerShuffledArray (int size) {
		int[] array = new int[size];
		for (int i = 0; i < size; ++i) array [i] = i;

		ShuffleArray<int> (array);

		return array;
	}

	public static void ShuffleArray<T> (T[] array) {
		for (int i = array.Length - 1; i >= 0; --i) {
			int j = Random.Range (0, i + 1);

			T temp = array [i];
			array [i] = array [j];
			array [j] = temp;
		}
	}

	public static void ShuffleList<T> (List<T> list) {
		for (int i = list.Count - 1; i >= 0; --i) {
			int j = Random.Range (0, i + 1);

			T temp = list [i];
			list [i] = list [j];
			list [j] = temp;
		}
	}

	public static void RestartListWithContent<T> (ref List<T> list, List<T> contentList, bool shuffle) {
		if (list == null)
			list = new List<T> ();
		else
			list.Clear ();

		for (int index = 0; index < contentList.Count; index++)
			list.Add (contentList [index]);

		if (shuffle)
			Util.ShuffleList<T> (list);
	}
}
