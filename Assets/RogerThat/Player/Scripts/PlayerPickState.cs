﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerPickState : PlayerState {

    private bool finishedPicking = false;
	private Vector3 lookDirection;

	private static int pickAnimation = Animator.StringToHash("Pick");

	private Message pickedMessage;

    public override bool CanEnter() {
        return this.player.carryingMessage == null && this.player.detector.message != null;
    }

    public override void Enter() {
		this.pickedMessage = this.player.detector.message;

		this.lookDirection = Vector3.Normalize(
			this.pickedMessage.transform.position - this.player.transform.localPosition);
		this.lookDirection.y = 0f;
		this.player.rigidbody.MoveRotation(Quaternion.LookRotation(this.lookDirection));

        this.player.animator.SetBool(pickAnimation, true);
        this.finishedPicking = false;
    }

	public override void FixedUpdate() {
		this.player.rigidbody.MoveRotation(Quaternion.LookRotation(this.lookDirection));
	}

    public override bool CanExit() {
        return this.finishedPicking;
    }

    public override void Exit() {
        this.player.animator.SetBool(pickAnimation, false);
        this.finishedPicking = false;
    }

    public void OnPickFinish() {
        this.finishedPicking = true;

		this.pickedMessage.OnPick(this.player.hand.transform);
		this.player.carryingMessage = this.pickedMessage;

		this.player.animator.SetBool(Player.carryingMessageHash, true);

        this.SwitchTo(this.player.idle);
    }
}
