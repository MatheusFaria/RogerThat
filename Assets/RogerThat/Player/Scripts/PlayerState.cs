﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerState {

	public bool enabled = true;
	[HideInInspector] public Player player;
	public string name;

	public bool IsStateExecuting { get { return this.player.state.name == this.name; } }

	public PlayerState() {
		this.player = null;
		this.name = "default_state";
	}

	public PlayerState(PlayerState playerState) {
		this.player = playerState.player;
		this.name = playerState.name;
	}

	public virtual void Init(Player player, string name="default_state") {
		this.player = player;
		this.name = name;
	}

	public virtual bool CanEnter() { return true; }
	public virtual void Enter() {}

	public virtual void FixedUpdate() {}
	public virtual void Update() {}

	// Dangerous Zone: Only override this method on extreme cases
	public virtual bool CanInterrupt() { return true; }
	public virtual void Interrupt() { this.Exit (); }

	public virtual bool CanExit() { return true; }
	public virtual void Exit() {}

	public void SwitchTo(PlayerState state) {
		// Can't change to the same state and can only change if you are the current state
		if (state.name == this.name || this.name != this.player.state.name || !state.enabled)
			return;

		if (this.CanExit() && state.CanEnter()) {
			//Debug.Log ("[STATES] Switching from " + this.name + " to " + state.name + " (" + this.player.GetInstanceID() + ")");
			this.player.state = state;
			this.Exit ();
			state.Enter ();
		}
	}

	public void ForceSwitchTo(PlayerState state) {
		// Can't change to the same state and can only change if you are the current state
		if (state.name == this.name || this.name != this.player.state.name || !state.enabled)
			return;

		if (this.CanInterrupt ()) {
			//Debug.Log ("[STATES] Force switching from " + this.name + " to " + state.name + " (" + this.player.GetInstanceID() + ")");
			this.player.state = state;
			this.Interrupt ();
			state.Enter ();
		}
	}
}
