﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJoystick : MonoBehaviour {

    [HideInInspector] public Player player;

    void Awake() {
        this.player = GetComponent<Player>();
    }

    void Update() {
        player.inputAxis.x = Input.GetAxis("Horizontal");
        player.inputAxis.z = Input.GetAxis("Vertical");

        player.actionTrigger = Input.GetButtonDown("Action");
    }
}
