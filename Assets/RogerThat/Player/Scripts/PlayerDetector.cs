﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetector : MonoBehaviour {

	[HideInInspector] public Message message;
	[HideInInspector] public MessagePlaySound messagePlaySound;
	[HideInInspector] public MessageSend messageSend;
	[HideInInspector] public MessageDrop messageDrop;

	public Slotable slot { get { return GetSlot (); }}

	private List<Slotable> slots;
	private Transform player;

	void Awake() {
		this.slots = new List<Slotable> ();
	}

	void Start() {
		this.player = GetComponentInParent<Player> ().transform;
	}

	void Update () {
//		Debug.Log ("slots: " + this.slots.Count);
	}

    void OnTriggerEnter(Collider collider) {
        if (collider.CompareTag("Message")) {
            message = collider.GetComponent<Message>();
        }
		else if (collider.CompareTag("Slotable")) {
			slots.Add(collider.GetComponent<Slotable>());
		}
		else if (collider.CompareTag("MessagePlaySound")) {
			messagePlaySound = collider.GetComponent<MessagePlaySound>();
		}
		else if (collider.CompareTag("MessageSend")) {
			messageSend = collider.GetComponent<MessageSend>();
		}
		else if (collider.CompareTag("MessageDrop")) {
			messageDrop = collider.GetComponent<MessageDrop>();
		}
    }

    void OnTriggerExit(Collider collider) {
		if (collider.CompareTag("Message")) {
            message = null;
		}
		else if (collider.CompareTag("Slotable")) {
			slots.Remove (collider.GetComponent<Slotable>());
		}
		else if (collider.CompareTag("MessagePlaySound")) {
			messagePlaySound = null;
		}
		else if (collider.CompareTag("MessageSend")) {
			messageSend = null;
		}
		else if (collider.CompareTag("MessageDrop")) {
			messageDrop = null;
		}
    }

	private Slotable GetSlot() {
		Slotable nearSlot = null;
		float minDistance = float.PositiveInfinity;

		for (int i = 0; i < slots.Count; ++i) {
			float distance = Vector3.SqrMagnitude (player.localPosition - slots[i].transform.position);
			if (distance < minDistance) {
				minDistance = distance;
				nearSlot = slots [i];
			}
		}

		return nearSlot;
	}
}
