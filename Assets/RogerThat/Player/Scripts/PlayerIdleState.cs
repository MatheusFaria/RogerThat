﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerIdleState : PlayerState {

    public override void FixedUpdate() {
        this.player.movement.FixedUpdate();
    }

    public override void Exit() {
        this.player.animator.SetBool("Run", false);
    }
}
