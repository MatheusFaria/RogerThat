﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public static int levelPushHash = Animator.StringToHash("LevelPush");
	public static int buttonPushHash = Animator.StringToHash("ButtonPush");
	public static int carryingMessageHash = Animator.StringToHash("CarryingMessage");

    public Vector3 inputAxis;
    public PlayerMovement movement;

    public bool actionTrigger = false;

    public Transform hand;

    [HideInInspector] public new Rigidbody rigidbody;
    [HideInInspector] public Animator animator;
    [HideInInspector] public PlayerDetector detector;

    [HideInInspector] public Message carryingMessage;
    [HideInInspector] public Machine machine;

    public PlayerState state;
    public PlayerIdleState idle;
    public PlayerPickState pick;
    public PlayerInteractState interact;

	void Awake () {
        this.rigidbody = GetComponent<Rigidbody>();
	}

    void Start() {
        this.animator = GetComponentInChildren<Animator>();
        this.detector = GetComponentInChildren<PlayerDetector>();

        this.movement.Init(this);

        this.idle.Init(this, "idle");
        this.pick.Init(this, "pick");
        this.interact.Init(this, "interact");

        this.state = this.idle;
    }

    void FixedUpdate() {
        state.FixedUpdate();
    }

    void Update() {
        state.Update();
    }

    void LateUpdate() {
        if (this.actionTrigger) {
			if (this.carryingMessage == null && this.detector.message != null &&
			             (this.detector.slot == null || this.detector.slot != null && this.detector.slot.Empty)) {
				this.state.SwitchTo (this.pick);

			} if (this.detector.slot != null) {
				this.state.SwitchTo (this.interact);

			} else if (this.detector.messagePlaySound != null) {
				this.detector.messagePlaySound.Play ();
				this.animator.SetTrigger(buttonPushHash);

			} else if (this.detector.messageSend != null) {
				this.detector.messageSend.Send ();
				this.animator.SetTrigger(levelPushHash);

			} else if (this.detector.messageDrop != null) {
				this.detector.messageDrop.DropAllMessages ();

			} else if (this.carryingMessage != null) {
				this.carryingMessage.OnRelase ();
				this.carryingMessage = null;
				this.animator.SetBool(carryingMessageHash, false);
			}
        }
    }
}
