﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {

    private Player player;

	void Start () {
		this.player = GetComponentInParent<Player>();
	}


    public void OnPickFinish() {
        this.player.pick.OnPickFinish();
    }

    public void OnPutMessageIn() {
        this.player.interact.OnPutMessageIn();
    }

    public void OnTakeMessageOut() {
        this.player.interact.OnTakeMessageOut();
    }

    public void OnPutInFinish () {
        this.player.interact.OnPutInFinish();
    }

    public void OnTakeOutFinish () {
        this.player.interact.OnTakeOutFinish();
    }
}
