﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerInteractState : PlayerState {

    private static int inAnimation   = Animator.StringToHash("MessageIn");
	private static int outAnimation  = Animator.StringToHash("MessageOut");

	private Vector3 lookDirection;


	public override bool CanEnter() {
		return this.player.detector.slot != null;
	}

    public override void Enter() {
		if (this.player.carryingMessage != null && this.player.detector.slot.Empty) {
			this.player.animator.SetBool(inAnimation, true);
		}
		else if (this.player.carryingMessage == null && this.player.detector.slot.canRemove) {
			this.player.animator.SetBool(outAnimation, true);
		}
		else {
			this.SwitchTo(this.player.idle);
			return;
		}

		this.lookDirection = Vector3.Normalize(
		this.player.detector.slot.transform.position - this.player.transform.localPosition);
		this.lookDirection.y = 0;

		this.player.rigidbody.MoveRotation(Quaternion.LookRotation(this.lookDirection));
		this.player.rigidbody.velocity = Vector3.zero;
    }

	public override void FixedUpdate() {
		this.player.rigidbody.MoveRotation(Quaternion.LookRotation(this.lookDirection));
	}

    public void OnPutMessageIn() {
		this.player.detector.slot.InsertMessage(this.player.carryingMessage);
		this.player.carryingMessage = null;
		this.player.detector.message = null;
    }

    public void OnTakeMessageOut() {
        this.player.carryingMessage = this.player.detector.slot.message;
        this.player.detector.slot.message.OnPick(this.player.hand);
        this.player.detector.slot.message = null;

		this.player.detector.slot.PlayGetOutSound ();
    }

	public void OnPutInFinish () {
		this.player.animator.SetBool(Player.carryingMessageHash, false);
		this.player.animator.SetBool(inAnimation, false);
        this.SwitchTo(this.player.idle);
    }

	public void OnTakeOutFinish () {
		this.player.animator.SetBool(Player.carryingMessageHash, true);
        this.player.animator.SetBool(outAnimation, false);
        this.SwitchTo(this.player.idle);
    }
}
