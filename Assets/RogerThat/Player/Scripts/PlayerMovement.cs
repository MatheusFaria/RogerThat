﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerMovement {

    public bool move = true;
    public float moveForce = 500f;
    public float maxVelocity = 5f;

    public bool rotate = true;
    public float rotationVelocity = 10f;

	[HideInInspector] public Player player;

    private Quaternion destinyRotation;
    private Vector3 lastRotation;

    private static int walkAnimation = Animator.StringToHash("Run");

    public void Init(Player player) {
        this.player = player;

        this.destinyRotation = this.player.transform.localRotation;
    }

    public void FixedUpdate() {
        Move();
        Rotate();
    }

    public void Move() {
        if (!this.move ||
            (this.player.inputAxis.x == 0 && this.player.inputAxis.z == 0)) {

            this.player.rigidbody.velocity = Vector3.zero;
            this.player.animator.SetBool(walkAnimation, false);
            return;
        }

         Vector3 direction = Vector3.Normalize(this.player.inputAxis);
         this.player.rigidbody.velocity = direction * maxVelocity;

//        player.rigidbody.AddForce(this.player.inputAxis * moveForce);
//
//        Vector3 velocity = player.rigidbody.velocity;
//        if (velocity.magnitude > maxVelocity) {
//            this.player.rigidbody.velocity = velocity.normalized * this.maxVelocity;
//        }

        this.player.animator.SetBool(walkAnimation, true);
    }

    public void Rotate() {
        if (this.player.transform.localRotation != this.destinyRotation) {
            this.player.rigidbody.MoveRotation(Quaternion.RotateTowards(
                this.player.transform.localRotation,
                this.destinyRotation,
                this.rotationVelocity
            ));
        }

        if (!this.rotate ||
            (this.player.inputAxis.x == 0 && this.player.inputAxis.z == 0)) return;

        this.lastRotation = Vector3.Normalize(this.player.inputAxis);
        this.destinyRotation = Quaternion.LookRotation(this.lastRotation);
    }
}
