%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Robot01@arms
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: anim_global
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_leftShoulder
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_leftShoulder/anim_LeftArm
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_leftShoulder/anim_LeftArm/anim_leftHand_GRP
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_leftShoulder/anim_LeftArm/anim_leftHand_GRP/anim_leftHand
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_leftShoulder/anim_LeftArm/anim_leftHand_GRP/anim_leftHand/ikHandle1
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_leftShoulder/anim_LeftElbow
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_leftShoulder_GRP
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_Neck
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_Neck/anim_neck_head
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_Neck/anim_neck_head/anim_head
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_rightShoulder
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_rightShoulder/anim_LeftElbow1
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_rightShoulder/anim_rightArm
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_rightShoulder/anim_rightArm/anim_rightHand_GRP
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_rightShoulder/anim_rightArm/anim_rightHand_GRP/anim_rightHand
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_rightShoulder/anim_rightArm/anim_rightHand_GRP/anim_rightHand/ikHandle2
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/anim_spine01/anim_torso/anim_rightShoulder_GRP
    m_Weight: 0
  - m_Path: anim_global/anim_wheelsBase/group11
    m_Weight: 0
  - m_Path: anim_global/bn_hip
    m_Weight: 0
  - m_Path: anim_global/bn_hip/bn_left_leg
    m_Weight: 0
  - m_Path: anim_global/bn_hip/bn_left_leg/be_leftWheel
    m_Weight: 0
  - m_Path: anim_global/bn_hip/bn_left_leg/be_leftWheel/joint9
    m_Weight: 0
  - m_Path: anim_global/bn_hip/bn_left_leg1
    m_Weight: 0
  - m_Path: anim_global/bn_hip/bn_left_leg1/be_leftWheel1
    m_Weight: 0
  - m_Path: anim_global/bn_hip/bn_left_leg1/be_leftWheel1/joint9 1
    m_Weight: 0
  - m_Path: anim_global/bn_hip/root
    m_Weight: 0
  - m_Path: anim_global/bn_hip/spine01
    m_Weight: 0
  - m_Path: anim_global/bn_hip/spine01/spine02
    m_Weight: 0
  - m_Path: anim_global/bn_hip/spine01/spine02/torso
    m_Weight: 0
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder/bn_leftArm
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder/bn_leftArm/bn_leftElbow
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder/bn_leftArm/bn_leftElbow/bn_leftHand
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder/bn_leftArm/bn_leftElbow/bn_leftHand/bn_leftFingers01
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder/bn_leftArm/bn_leftElbow/bn_leftHand/bn_leftFingers01/bn_leftFingers02
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder/bn_leftArm/bn_leftElbow/bn_leftHand/bn_leftFingers01/bn_leftFingers02/be_leftFingers01
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder/bn_leftArm/bn_leftElbow/bn_leftHand/bn_leftThumb01
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder/bn_leftArm/bn_leftElbow/bn_leftHand/bn_leftThumb01/bn_leftThumb02
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder/bn_leftArm/bn_leftElbow/bn_leftHand/bn_leftThumb01/bn_leftThumb02/be_leftThumb01
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder1
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder1/bn_leftArm 1
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder1/bn_leftArm 1/bn_leftElbow
      1
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder1/bn_leftArm 1/bn_leftElbow
      1/bn_leftHand 1
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder1/bn_leftArm 1/bn_leftElbow
      1/bn_leftHand 1/bn_leftFingers01 1
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder1/bn_leftArm 1/bn_leftElbow
      1/bn_leftHand 1/bn_leftFingers01 1/bn_leftFingers02 1
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder1/bn_leftArm 1/bn_leftElbow
      1/bn_leftHand 1/bn_leftFingers01 1/bn_leftFingers02 1/be_leftFingers01 1
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder1/bn_leftArm 1/bn_leftElbow
      1/bn_leftHand 1/bn_leftThumb01 1
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder1/bn_leftArm 1/bn_leftElbow
      1/bn_leftHand 1/bn_leftThumb01 1/bn_leftThumb02 1
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/bn_leftShoulder1/bn_leftArm 1/bn_leftElbow
      1/bn_leftHand 1/bn_leftThumb01 1/bn_leftThumb02 1/be_leftThumb01 1
    m_Weight: 1
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/neck
    m_Weight: 0
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/neck/head
    m_Weight: 0
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/neck/head/Head
    m_Weight: 0
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/neck/head/LeftEye 1
    m_Weight: 0
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/neck/head/LeftEye 1/LeftEyeEnd
    m_Weight: 0
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/neck/head/RightEye 1
    m_Weight: 0
  - m_Path: anim_global/bn_hip/spine01/spine02/torso/neck/head/RightEye 1/RightEyeEnd
    m_Weight: 0
  - m_Path: anim_global/nurbsCircle1
    m_Weight: 0
  - m_Path: anim_global/nurbsCircle1/anim_leftWheel
    m_Weight: 0
  - m_Path: anim_global/nurbsCircle1/ikHandle3
    m_Weight: 0
  - m_Path: anim_global/nurbsCircle2
    m_Weight: 0
  - m_Path: anim_global/nurbsCircle2/anim_rightWheel
    m_Weight: 0
  - m_Path: anim_global/nurbsCircle2/ikHandle3 1
    m_Weight: 0
  - m_Path: anim_global/nurbsCircle2/ikHandle4
    m_Weight: 0
  - m_Path: Body
    m_Weight: 1
  - m_Path: Body/Body 1
    m_Weight: 1
  - m_Path: Body/LeftEye
    m_Weight: 1
  - m_Path: Body/RightEye
    m_Weight: 1
  - m_Path: Body/Wheels
    m_Weight: 1
